# Ossin Lassyt / Ossis Ramblings
This repository houses the book "Ossin Lassyt" and contains tools to build an .epub- and .html-versions of said book.
There is also an ongoing translation project, instructions to participate in that below.

## How to contribute to the translation project
1. Choose a file to translate in finnish/lassyt/
2. Make an issue in [GitLab](https://version.aalto.fi/gitlab/ossinlasyt/ossinlasyt/issues/new) and mark yourself as the assignee.
3. Produce the translation and apply it in the folder english/ramblings/ to the corresponding file.
4. Push the translated file to a new branch in gitlab.
5. Make a merge request, you can use the link gitlab provides you when you push stuff to the new branch.
6. Discussion about the translation can be performed in the MR itself.
7. Actual merging will happen once all of the discussion is resolved.
8. Return to step 1.

## Requirements
* GNU make
* [pandoc](https://pandoc.org/)

### Editors note

Nämä lässyt löytyivät AYY:n ylläpitämältä verkkolevyltä siinä muodossa kun ne olivat ensimmäisessä commitissa. Gittiä varten poistettiin turhaa tauhkaa ja muokattiin tiedostoja moderniin Markdown-formaattiin. Itse tekstisitältöihin ei ole muutamia merkistökorjauksia lukuun ottamatta koskettu.

The eBook-conversion has been copied from [here](https://github.com/jagregory/abrash-black-book).
