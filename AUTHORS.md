# Mission log: stardate 00023092 

Me, Petri Strandén ja Jarno Alm, olemme tällä päivämäärällä, 29.3.2000, Teknillisen korkeakoulun päärakennuksen ensimmäisessä kerroksessa sijaitsevassa Unix-luokassa U132 (Uranus),  tietokoneen U13201 äärellä, ahkeroineet pääainettamme: Ossin lässyjen skannaus ja word-dokumentointi.
Fukseille suunnattuun sähköiseen (e-mail) tietopläjäykseen oli sisällytetty mahdollisuus suorittaa pääaine Anna Palosen ankarassa alaisuudessa.
Seksinnälkäisinä, vailla pääaineen fuksipisteitä ja tietoisina siitä, että avaruudessa olevat vihamieliset tahot yrittäisivät sabotoida tehtäväämme, päätimme riskeerata olemassaolomme ja ottaa tehtävän vastaan.

Petri Strandén, Ph.D        
Jarno Alm, Ltn. Colonel

## Nämä läsyt on digitoitu seuraavien ihmisten toimesta.

| Läsyt               | Digitoijat       |
| ------------------- | ---------------- |
| 9-41                | Lasse Koskela    |
| 51-61               | Juho Seppänen    |
|                     | Paavo Leskinen   |
| 41-50               | Jussi Kuutti     |
|                     | Jussi Heimo      |
| 20-30               | Antti Lehtonen   |
|                     | Juha Manninen    |
| 62-75               | Jarno Alm        |
|                     | Petri Stranden   |
| 43,36,91-104        | Anna Liukko      |
| alku, kylä, 105-127 | Juha Kainulainen |
| 76-90               | Tuukka Kostamo   |
| 128-149             | Mikael Runonen   |



### Editors note

Nämä lässyt löytyivät AYY:n ylläpitämältä verkkolevyltä siinä muodossa kun ne olivat ensimmäisessä commitissa. Gittiä varten poistettiin turhaa tauhkaa ja muokattiin tiedostoja moderniin Markdown-formaattiin. Itse tekstisitältöihin ei ole muutamia merkistökorjauksia lukuun ottamatta koskettu.

The eBook-conversion has been copied from [here](https://github.com/jagregory/abrash-black-book).
