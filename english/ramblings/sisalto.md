
---
title: Ossin Lässyt
author: Ossi Törrönen
---

    
# SISÄLLYSLUETTELO

1. Saapumiseni Otaniemeen vappuna. Teekkarien tiilitalkoot.
2. Koko yön kestänyt Vappuserenadi.
3. Teekkarikylän vihkiäiset.
4. Ruokalaparakki häipyy yhdessä yössä.
5. Kipusiskokisat alkavat.
6. Syön karvalakkini.
7. Eduskunnan valtiovarainvaliokunnan vierailu.
8. Juhannusjuhlat.
9. Terveydenhoitoasema perustetaan.
10. Villieläimiä Otaniemessä.
11. Ankerias.
12. Python-käärme.
13. Linjurimme Sulo Riento Liukas.
14. Trojan autolla yleislakossa.
15. Järjestys kylässä.
16. Rakennusmestari riehuu kylässämme.
17. Kairaaja sammuu rantasaunaamme.
18. Paloautomme Suihku-Siwiän koeruiskutus.
19. THS:n 204-senttinen vuosijuhlavieras.
20. Toronton teekkarit ja rantasaunan vihkiäiset.
21.Caledon-kanootti.
22. Jätepaperin keräys Unkarille.
23. Kipusiskojen lahjaesineitä. Erilaisia kilpailulajeja.
24. Pentti Kerola ja kumiköysilinko.
25. Kuorma-automme NALLE-PUH kastetaan.
26. Paloauto SUIHKU-SIWIÄ saa uudet renkaat.
27. TEMPAUKSIA. J. L. Runeberg ja Kihniön kirkkoherra.
28. Lehmän hitsaaminen valtioneuvoston porttiin.
29. Kylän vuohet Maija ja Teemu.
30. Kylän 5-vuotispaukku progressiivisin mallein.
31. Kwai-joen junttauslaulu.
32. Olympiakesä 1952.
33. USA:n jalkapallojoukkue rantasaunalla.
34. RWBK:n palokello ja muita kuumia tapahtumia.
35. Kätilökongressi.
36. Polterabend-teekkarin jalka pakettiin.
37. Olin pannukakku AD 1882.
38. Otaniemen Viininviljelijät.
39. Anastasia Mikojan vierailee Servin Mökissä.
40. Huoneeseen muurattu väliseinä ja muita dekoraatioita.
41. Onni Vaara ja kanadalaisvaahtera.
42. TKYH:n vaihtoseremoniat vanhalla Polilla.
43. Tämä ei ole valkoisen miehen soppaa.
44. Perintösohvani polttaminen.
45. Kanoottiviestit Flooran päivänä Helsinkiin.
46. Puutarhuri Helling lahjoittaa 5.000 kukkasipulia.
47. Keskusradio talkoilla kuntoon.
48. Poliisiopisto tunkeutuu Otaniemeen.
49. Teekkari maksoi ikkunan etukäteen.
50. Arvilommin solu poltti pöytänsä tunnesyistä.
51. Otagorsun rakennustalkoot kun kaljaa riitti.
52. Kappelin leivinuuni kovassa käytössä. Minua luultiin Jeesukseksi.
53. Servin Mökin varkaiden huono onni.
54. TKYH kokeilee Servin hälytyslaitteita.
55. Rantasaunan hälytyslaitteet ja teekkari Veisterän polttis.
56. Dipolin puunkaatotalkoot.
57. Teekkari Teemu Tynkkynen ilmapallolla taivaalle.
58. "Jämerän silmä" -patsas ilmestyy Otaniemeen.
59. Rehtori Jaakko Rahola eksyy omaan korkeakouluunsa.
60. Juna-tempaus 7:ään kaupunkiin.
61. Tempaus Lappeenrannan oluttehtaalla.
62. Paavo Nurmi -patsas Wasa-laivassa.
63. Luolamiesten kunniamolkku.
64. TKYH tutustuu atomiluolaan ja puliukkoon.
65. Ihmeitä tekevä lähde atomiluolassa.
66. Ohmimakkaran valmistusohje.
67. Tekninen koe sadevesitorvessa.
68. Täytän 50 vuotta.
69. Teekkareilta saamiani arvolahjoja.
70. Teekkarit kantavat omaa porttiaan.
71. Polterabend-teekkari jäi yksin junaan nakuna.
72. Teekkarit alkavat osallistua kunnalliselämään.
73. Espoo täyttää 500 vuotta. TKY:lle "milli".
74. TKY:n edustajiston ensimmäiset vaalit.
75. Maan siat ja kokoomus.
76. Pekka Tarjanteen häät.
77. Hamburgin tekn.messut ja RWBK
78. Ensimmäinen T-ilta Dipolissa. KOFF tarjosi.
79. Kilpajuoksu öisellä jäällä.
80. Teekkarista oli tulla Diapositiivi.
81. Lauri leivän söi.
82. Lasimestarin silli tuli rantasaunaan.
83. Virkaatekevä puliukko lämpökanavissa.
84. Juridinen persoona = 3'/i milj. markkaa.
85. Kaksi rauhankongressia samaan aikaan.
86. Sukkasuhinat ja minä kännissä.
87. Ranskattaret ja Hollannin matruusit saunarannalla.
88. Radiolupatarkastus.
89. PLH vaati keskusradion joka pisteeseen eri luvan.
90. Sain kultakellon kylän teekkareilta.
91. DIPOLIN synnytystuskat.
92. UKK Dipolissa. Kävyn käynnistys.
93. Pekka Tarjanteen mukana Chalmersissa.
94. Ruotsin nyk. kuningas saa RWBK:lta äänilevyn.
95. Tanskan prinsessa Otaniemessä.
96. Heikki Sirenin ja Leino Hassisen kaksintaistelut.
97. Servin Maija yritti pönkittää puliukkoa oikealle tielle.
98. Kappelin viherkasvijupakka.
99. Brittiläinen teekkarijäynä.
100. Ratikka hitsattiin kiskoihin Chalmersissa.
101. Antero Salmenkiveltä kädet ylös.
102. DIPOLIINAN synty.
103. Ilmainen soitto kaukotasolle kylästä.
104. TES-TV aloittaa sähkölafkan vinnillä.
105. Vappuriehaa TES-TV:ssä.
106. Teekkari ammuttiin kylän savupiipusta avaruuteen.
107. Tutsan vappuvarmistus.
108. Servin Maijan makkaranteon MM
109. Doora vei Ossia ulos.
110. Kiinan suurlähetystön luumuviinat.
111. Polin emäntä käryää oluesta.
112. UKK:kin sai odottaa cocktailiaan Dipolissa.
113. DIPOLI-nimen synty.
114. Dipolin ensimmäinen T-ilta, Pikku-Joulut ja Uusi Vuosi.
115. Poliisiopiston jäynäsielut. Katila, Aas, Jotuni.
116. Ensimmäinen hyökkäyksemme poliisiopistoon.
117. Poliisiopistosta kätilöopisto.
118. Käsiraudoissa poliisien saunasitsiin. Nappi paltostani.
119. Knut Aas toi minulle naispoliiseja.
120. Lähetämme lahjapaketin poliisiopistolle.
121. Taistelu poliisien hyppylaudasta. Vaihdamme Elorannan autoon.
122. Kännikala poliisineuvos Elorannalle.
123. Poliisioppilaat lunastavat oluttrattamme KOP:sta.
124. Poliisioppilaat soluttavat oman Otahuutonsa kylään.
125. Esittelemme Dipoliamme poliisiopiston johdolle.
126. Poliisien järjestämä oikeudenkäynti opistolla.
127. Ylin poliisijohto ajelee Porscheilla atomiluolassa.
128. Luetteloa poliisien antamista lahjakaluista.
129. Eversti Lyytisen valtakausi Teekkarikylässä.
130. Luolamiehen avajaiset kera kolmen stripparin.
131. Kalakukkokellari ja Peräkylän Banjot.
132. Tinteron valmistujaiset kestivät pari viikkoa.
133. Tippavaaran isäntä vierailee kylässä.
134. Dipolin asiakaspaikkaluku ja anniskelualueet.
135. Dipoli auki yli yön kera cocktail-automaatin.
136. Juhlat Servissä ilman lupaa - Minä kihlakunnan oikeuteen.
137. Kirkonkello akkunani ulkopuolella.
138. Läksiäisjuhlani Otaniemestä 1969.
139. Täytän 60 v. 2. 4. 1975. Irma junailee hipat.
140. Kansaneläke alkaa juosta 2. 4. -81 Irmalla taas sormet pelissä.
